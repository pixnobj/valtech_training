//login.js
var authMod = require('authMod');

authMod.authenticate('satish', 'chandra', onSuccess, onError);

console.log("Authentication initiated....");

function onSuccess (userInfo) {
    console.log('\tAuthentication success for user: '+ userInfo.userName);
}

function onError(err){
    console.log(err.details);
}