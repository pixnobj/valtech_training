//math.js

function add(x, y) {
    return (x + y);
}
function div(x, y) {
    return (x / y);
}

//exposing module api's
module.exports = {
    sum: add,
    div: div
};