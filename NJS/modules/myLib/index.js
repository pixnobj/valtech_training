//index.js
function greet(name) {
    console.log('Hi' + name + ', Welcome to NodeJS!');
}

var math = require('./math');

module.exports = {
    message: greet,
    div: math.div,
    sum: math.add
};