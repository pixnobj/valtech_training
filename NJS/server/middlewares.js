//middlewares.js

function writeSign(name, sign) {
    return function (req, resp, next) {
        resp.setHeader (name, sign);
        next();
    };
}

function greet() {
    return function (req, resp, next) {
        console.log('\t Writing message');
        resp.write('<h1>Valtech, Bangalore</h1>');
        resp.write('<img src="./fish.jpg" />');
        next();
    }
}

function finish (req, res) {
    res.end('<strong>....End</strong>');
}

module.exports = {
    writeSignature: writeSign,
    greet: greet,
    finish: finish
}