//httpServer.js
//load core module - "http"

var http = require('http');

/**
 * create basic server
 * @param port
 */
function createBasicServer (port) {
    // create server instance, i.e http listener
    var server = http.createServer();
    //Event call back for new http request
    server.on('request', function(req, res){
        console.log('\tHandling new request..');
        res.write('<h1>Valtech HTTP Server..!');
        res.end();
    });
    //start server on port
    server.listen(port);
    console.log("Valtech HTTP Server running @ " +port);
}

function createServerStack (port) {
    //load third party npm instance
    var connect = require('connect');
    //create server instance
    var serveStatic = require('serve-static');
    var server = connect();
    //load middlewares
    var middlewares = require('./middlewares');
    // configure middlewares
    server.use(middlewares.writeSignature('x-PoweredBy','Valtech, Blr'));
    server.use(serveStatic('./'));
    server.use(middlewares.greet());
    server.use(middlewares.finish);
    server.listen(port);
}
createServerStack(9191);
//createBasicServer(9090);