//create a web server
var connect = require('connect');
var staticServe = require('serve-static');
// configure server to serve static files
var app = connect();
app.use(staticServe('./public'));
var server = require('http').Server(app);
//Websocket server login using "socket.io"
var sio = require("socket.io")(server);
var clients = [];

//listener to accept new connection
sio.on('connection', function(socket){
    //add new client to clients list
    clients.push(socket);
    //register events on client channel
    socket.on('GM', function (name){
        console.log('Client: (GM) - ' + name);
        socket.send('Hi '+ name + ', have a nice day');
    });

    socket.on('GE', function(name){
        console.log('Client: (GE) - ' + name);
        socket.send('Hi ' + name + ', Good Evening!');
    })
});
server.listen(9000);