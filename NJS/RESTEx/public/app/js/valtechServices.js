//valtechServices.js
angular.module('valService', [])
    //--------------------------FILTERS
    .filter('tagDays', [function () {
        return function (days) {
            var output = '';
            if (days < 2)
                output = 'You have only few hours!';
            else if (days <= 7)
                output = 'Only ' + days + 'days left!';
            else
                output = 'Come back for this within ' + days + 'days';

            return output;
        }
    }])
    .filter('highReviewed', [function () {
        return function (products) {
            var output = [];
            angular.forEach(products, function (prod) {
                //enable only when the response data containing reviews
                /*if (prod.Reviews && prod.Reviews.length > 1) {
                    output.push(prod);
                }*/
                output.push(prod);
            });
            return output;
        }
    }])
    //--------------------------DIRECTIVES
    //USAGE: <div val-list="Products" listField="Name" ....
    .directive('valList', function () {
        return function (scope, elem, attrbs) {
            var listId = attrbs.valList;
            var field = attrbs.listfield;
            //add $watch to tract active product change
            scope.$watch(listId, function (newV, oldV) {
                generateList(scope, elem, field, listId);
            }, true);
        };
        /**
         *
         * @param scope
         * @param elem
         * @param field
         * @param items
         */
        function generateList(scope, elem, field, listId) {
            //generate ul and li elements
            var items = scope.$eval(listId, scope);
            elem.empty();
            var ul = angular.element('<ul>');
            ul.addClass('list-group');
            elem.append(ul);
            // generate list items
            if (angular.isDefined(items) && angular.isArray(items)) {
                angular.forEach(items, function (item) {
                    var li = angular.element('<li>');
                    li.addClass('list-group-item');
                    var text = scope.$eval(field, item);
                    li.text(text);
                    ul.append(li);
                })
            }
        }
    })
    .directive('cartInfo', function () {
        //DDO
        return {
            restrict: 'E', //Only as element
            templateUrl: 'app/templates/cart.html',
            transclude: true,
            controller: ['$scope', function(scope) {
                scope.cart = {
                    customerName: 'Jim Carry',
                    items: [
                        {name: 'Mask', price: 10500, qty: 5},
                        {name: 'Sneakers', price:950, qty: 10},
                        {name: 'iPhone 6', price:100000,qty: 2}
                    ]
                };
            }]
        }
    });
    /*.directive('dname', [function () {
        //Directive Definition Object
        return {
            compile: function ($compile) {},
            restrict: 'ECMA',
            link: function (scope, elem, attrbs) {},
            replace: true,
            controller: function(){},
            scope: {},
            template: '<ul></ul>',
            templateUrl: 'path of the template file',
            transclude: true
        }
    }]);*/