/**
 * Module definition
 */
angular.module('mainMod', ['ngMessages','valService'])

    /**
     * Controller definition
     */
    .controller('productsCtrl', function ($scope, ProductsData, $rootScope) {
        //Assigning products data to the Scope variable Products
        //$scope.Products = JSON.parse(ProductsData);
        //$rootScope.Product = $scope.Products[0];
        $scope.selectProduct = function(product){
            // assign selected product to rootScope
            $rootScope.Product = product;
        };
    })
    .controller('tabCtrl', function($rootScope){
        //initialize
        $rootScope.tab = 1;
        this.tab = $rootScope.tab;
        //set tab index
        this.setTab = function(idx){ this.tab = idx; };
        //check tab index
        this.isTab = function (idx){return (this.tab == idx);};
    })
    .controller('reviewCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {
        $scope.review = {};
        //save new review details
        $scope.saveReview = function(){
            if( $rootScope.Product) {
                $rootScope.Product.Reviews.push($scope.review);
                $scope.review = {};
            }
        }
    }])
    .run(['$rootScope', 'ProductsData','$http', function(rtScope, products, $http){
        $http.get('/products')
            .success(function(result){
                rtScope.Products = result;
                rtScope.Product = rtScope.Products[0];
            })
            .error(function (err) {
                alert(err);
            })
        //rtScope.Product = JSON.parse(products)[0];
    }]);
