// spaMod.js

angular.module('spaMod', ['ngRoute'])
//-----------Controllers
.controller('booksCtrl', ['$scope', 'dataService', function (scope, dataService) {
	scope.Books = [];
	
	scope.searchBooks = function () {
		// retrive books list from server based on search criteria
		dataService.searchBooks(scope.query)
		.success(function (result) {
			scope.Books = result.Books;
		})
		.error(function (err) {
			alert(err.message);
		});
	};
}] )

.controller('detailsCtrl', ['$scope', '$routeParams', 'dataService', function (scope, $routeParams, dataService) {
	scope.Book = [];
	//get book details based on the book id from the url
	dataService.getDetails($routeParams.id)
			.success(function(result){
				scope.Book = result;
			})
			.error(function(err){
				alert(err.message);
			})
}] )

//-----------Services
.service('dataService', ['$http', function (http) {
	var sUrl = 'http://it-ebooks-api.info/v1/search/'
	var bUrl = 'http://it-ebooks-api.info/v1/book/'

	this.searchBooks = function (query) {
		return http.get(sUrl + query);
	};
	this.getDetails = function(id) {
		return http.get(bUrl + id);
	};
}])

//-----------Cofigure
.config(['$routeProvider', function (routeProvider) {
	//configure urls with templates and controllers
	routeProvider.when('/', {templateUrl: 'app/templates/home.html'})
	.when('/books', {templateUrl: 'app/templates/books.html', controller: 'booksCtrl'})
	.when('/book/:id', {templateUrl: 'app/templates/book-details.html', controller: 'detailsCtrl'})
	.otherwise({ redirectTo: {templateUrl: '/'}});
}]);