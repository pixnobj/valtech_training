var express = require('express');
var router = express.Router();

//Configure http (verbs) for /products url

//Create mysql connection
function getConnection() {
    var mysql = require('mysql');
    // crate connection
    var cnn = mysql.createConnection({
        host: 'localhost',
        database: 'catalog',
        user: 'root'
    });

    return cnn;
}

router.get('/',function(req, res, next){
    var cnn = getConnection();
    //retrieve product data
    cnn.query('SELECT * FROM products', function(err, results, fields){
        //console.log(results);
        res.json(results);
        //res.render('products',{products: results});
    });
    //res.render('products',{title: "Valtech"});
});

router.post('/', function(req, res, next){
   // Insert products
});

router.delete('/', function(req, res, next){

});

module.exports = router;